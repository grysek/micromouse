#include "virtualmouse.h"
#include <random>

std::shared_ptr<Point<int>> VirtualMouse::GetCurrentPoint()
{
    return currentPoint;
}

void VirtualMouse::UseDistanceEvalFunc()
{
    evalFunc = Distance;
}

void VirtualMouse::UseRandomEvalFunc()
{
    evalFunc = Random;
}

double VirtualMouse::Evaluate(Point<int> &destination)
{
    if(evalFunc == Random)
        return ((double)std::rand())/(double)RAND_MAX;
    return destination.Distance(labirynth.GetEnd());
}

bool VirtualMouse::CanMove(Point<int> &destination)
{
    return labirynth.GetField(destination) != Wall;
}

void VirtualMouse::Move(Point<int> &destination)
{
    currentPoint = std::make_shared<Point<int>>(destination);
}
