#include "micromouseproblem.h"
#include "micromousestate.h"
#include <string>

std::shared_ptr<State> MicromouseProblem::GetRootState()
{
    return std::make_unique<MicromouseState>(labirynth.GetBegin());
}

double MicromouseProblem::Evaluate(State &state)
{
    MicromouseState& ms = static_cast<MicromouseState&>(state);
    return mouse.Evaluate(ms.getPoint());
}

bool MicromouseProblem::IsTheGoal(State &state)
{
    MicromouseState& ms = static_cast<MicromouseState&>(state);
    return ms.getPoint() == labirynth.GetEnd();
}

double MicromouseProblem::CostOfMove(State &current, State &target)
{
    MicromouseState& mc = static_cast<MicromouseState&>(current);
    MicromouseState& mt = static_cast<MicromouseState&>(target);
    return mc.getPoint().Distance(mt.getPoint());
}

void MicromouseProblem::TranslateSolution(std::deque<std::shared_ptr<State>> solution)
{
    result.clear();

    for(auto state: solution)
    {
        MicromouseState& ms = static_cast<MicromouseState&>(*state);
        result.push_back(std::make_shared<Point<int>>(ms.getPoint()));
    }
}

void MicromouseProblem::GoIfPossible(Point<int>& point, std::vector<std::shared_ptr<State>>& options)
{
    if(mouse.CanMove(point))
        options.push_back(std::move(std::make_shared<MicromouseState>(point)));
}

std::deque<std::shared_ptr<Point<int>>> MicromouseProblem::GetResult()
{
    return result;
}

std::shared_ptr<MicromouseState> MicromouseProblem::GetCurrentState()
{
    return current;
}

std::vector<std::shared_ptr<State>> MicromouseProblem::GetSuccessors(State &state)
{
    MicromouseState& ms = static_cast<MicromouseState&>(state);
    auto point = ms.getPoint();

    std::vector<std::shared_ptr<State>> options;

    if(point.getX() < (labirynth.GetSize() - 1))
        GoIfPossible(Point<int>(point.getX() + 1, point.getY()), options);
    if(point.getX() > 0)
        GoIfPossible(Point<int>(point.getX() - 1, point.getY()), options);
    if(point.getY() < (labirynth.GetSize() - 1))
        GoIfPossible(Point<int>(point.getX(), point.getY() + 1), options);
    if(point.getY() > 0)
        GoIfPossible(Point<int>(point.getX(), point.getY() - 1), options);

    return options;
}


void MicromouseProblem::SelectState(State &state)
{
    MicromouseState& ms = static_cast<MicromouseState&>(state);
    current = std::make_shared<MicromouseState>(ms);
    mouse.Move(current->getPoint());
}
