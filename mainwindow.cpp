#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "astar.h"
#include "micromouseviewmodel.h"
#include <QTimer>
#include <QGraphicsRectItem>
#include <QFileDialog>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    viewModel.SetSpeed(5);

    scene = new QGraphicsScene();
    ui->graphicsView->setScene(scene);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(refresh()));
    timer->start(50);
}

MainWindow::~MainWindow()
{
    delete timer;
    delete scene;
    delete ui;
}

void MainWindow::refresh()
{
    ui->resultLabel->setText(QString::fromStdString(viewModel.GetStatus()));
    ui->timeLineEdit->setText(QString::fromStdString(viewModel.GetTime()));
    ui->positionLineEdit->setText(QString::fromStdString(viewModel.GetPosition()));
    ui->lengthLineEdit->setText(QString::fromStdString(viewModel.GetPathLength()));
    ui->costLineEdit->setText(QString::fromStdString(viewModel.GetCost()));
    ui->delayLabel->setText(QString::fromStdString(viewModel.GetDelayText()));
    ui->visitedLineEdit->setText(QString::number(viewModel.GetVisitedNumber()));
    ui->loadButton->setEnabled(viewModel.CanLoad());
    ui->startButton->setEnabled(viewModel.CanStart() || viewModel.CanPause());
    ui->stopButton->setEnabled(viewModel.CanStop());


    if(viewModel.ShouldReloadEvalFuncOptions())
    {
        auto opt = viewModel.GetEvalFuncOptions();
        for(int i = 0; i < opt.size(); i++)
            ui->comboBox->insertItem(i, QString::fromStdString(opt[i]));
    }

    if(viewModel.CanStart())
        ui->startButton->setText("Start");
    if(viewModel.CanPause())
        ui->startButton->setText("Pause");

    if(viewModel.LabirynthChanged())
        DrawLabirynth();

    if(viewModel.PositionChanged())
        DrawMouse();

    if(viewModel.ShouldDrawSolution())
    {
        for(auto& point : viewModel.GetSolution())
        {
            DrawBlock(point->getX(), point->getY(), Qt::green);
        }
    }
}

void MainWindow::DrawBlock(int x, int y, QColor color)
{
    QGraphicsRectItem* block = new QGraphicsRectItem(x*fieldSize,y*fieldSize,fieldSize,fieldSize);
    block->setBrush(QBrush(color));
    scene->addItem(block);
}

void MainWindow::DrawLabirynth()
{
    scene->clear();

    auto& l = viewModel.GetLabirynth();

    for(int x = 0; x < l.GetSize(); x++)
    {
        for(int y = 0; y < l.GetSize(); y++)
        {
            if(l.GetField(Point<int>(x,y)) == Wall)
            {
                DrawBlock(x, y, Qt::black);
            }
        }
    }

    DrawBlock(l.GetBegin().getX(),l.GetBegin().getY(), Qt::red);
    DrawBlock(l.GetEnd().getX(),l.GetEnd().getY(), Qt::yellow);

    scene->update();
}

void MainWindow::DrawMouse()
{
    auto& point = viewModel.GetCurrentPoint();

    if(viewModel.HasPreviousPoint())
    {
        auto& prevPoint = viewModel.GetPreviousPoint();
        DrawBlock(prevPoint.getX(), prevPoint.getY(), Qt::gray);
    }

    DrawBlock(point.getX(), point.getY(), Qt::red);
}

void MainWindow::on_loadButton_clicked()
{
    auto fileName = QFileDialog::getOpenFileName(this,
        tr("Open Labirynth"), ".", tr("Bitmap Files (*.bmp)"));
    if(fileName.length() > 0)
        viewModel.LoadLabirynthFromFile(fileName);
}

void MainWindow::on_startButton_clicked()
{
    viewModel.Run();
}

void MainWindow::on_stopButton_clicked()
{
    viewModel.Stop();
}

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    viewModel.SetSpeed(value);
}

void MainWindow::on_comboBox_currentTextChanged(const QString &arg1)
{
    viewModel.SelectEvalFunc(arg1.toStdString());
}
