#ifndef ASTAR_H
#define ASTAR_H

#include <stdlib.h>
#include <iproblem.h>
#include <state.h>
#include <set>
#include <deque>
#include <memory>
#include <chrono>
#include <thread>

/*! Komparator potrzebny do porownywania elementów setu (wskaznikow) */
struct stateComparer {
    bool operator() (const std::shared_ptr<State>& lhs, const std::shared_ptr<State>& rhs) const {
        return *lhs > *rhs;
    }
};

/*! Klasa realizujaca algorytm A*, rozwiazujaca dowolny problem zdefiniowany przez IProblem */
class AStar
{
public:
    AStar();
    bool Resolve(IProblem& problem);
    void Stop();
    
    bool IsPaused() const;
    void SetPaused(bool value);
    void SetDelayTime(int microseconds);
    int GetVisitedStatesNumber();

private:
    std::set<std::shared_ptr<State>, stateComparer> openSet;
    std::set<std::shared_ptr<State>, stateComparer> closedSet;
    bool stopRequest = false;
    bool paused = false;
    std::chrono::milliseconds delayTime;

    bool IsInOpen(std::shared_ptr<State>& state);
    bool IsInClosed(std::shared_ptr<State>& state);
    std::deque<std::shared_ptr<State>> ReversePath(std::shared_ptr<State>& node);
    std::deque<std::shared_ptr<State>> decisionNodes;
    std::shared_ptr<State> GetBest(IProblem& problem, std::shared_ptr<State>& state);
    void Wait();
    void BacktrackIfNeeded(IProblem& problem, std::shared_ptr<State>& current, std::shared_ptr<State>& last);
    void RemoveChildrenFromOpen(std::shared_ptr<State>& last);
};

#endif // ASTAR_H
