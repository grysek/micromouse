#include "bitmaplabirynth.h"
#include <QColor>
#include <random>

Point<int> BitmapLabirynth::GetRandomPoint()
{
    return Point<int>(std::rand() % this->source->width(), std::rand() % this->source->width());
}

void BitmapLabirynth::GetRandomBegin()
{
    while (true)
    {
       Point<int> p = GetRandomPoint();
       if(GetField(p) != Wall)
       {
           begin = std::make_unique<Point<int>>(p);
           break;
       }
    }
}

void BitmapLabirynth::GetRandomEnd()
{
    while (true)
    {
        Point<int> p = GetRandomPoint();
        if(GetField(p) != Wall && p.Distance(*begin) > (this->source->width()))
        {
            end = std::make_unique<Point<int>>(p);
            break;
        }
    };
}

BitmapLabirynth::BitmapLabirynth(QImage source)
{
    if(!ValidateSize(source.width()) || source.width() != source.height())
        throw std::invalid_argument("Source image has wrong dimensions.");
    this->source = std::make_unique<QImage>(source);

    for(int x = 0; x < this->source->width(); x++)
    {
        for(int y = 0; y < this->source->width(); y++)
        {
            QColor pixel = this->source->pixelColor(x, y);
            if(pixel == Qt::red)
                begin = std::make_unique<Point<int>>(x, y);
            else if(pixel == Qt::blue)
                end = std::make_unique<Point<int>>(x, y);
        }
    }

    if(!begin)
        GetRandomBegin();

    if(!end)
        GetRandomEnd();
}

bool BitmapLabirynth::ValidateSize(int size)
{
    return size < 33 && size > 3;
}

LabirynthField BitmapLabirynth::GetField(const Point<int> &point)
{
    QColor pixel = source->pixelColor(point.getX(), point.getY());
    if(pixel == Qt::black)
        return Wall;
    return Normal;
}

Point<int> BitmapLabirynth::GetBegin()
{
    return *begin;
}

Point<int> BitmapLabirynth::GetEnd()
{
    return *end;
}

int BitmapLabirynth::GetSize()
{
    return source->width();
}
