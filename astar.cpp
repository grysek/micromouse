#include "astar.h"
#include "iproblem.h"
#include "state.h"
#include <algorithm>

using namespace std::chrono_literals;

AStar::AStar()
{
    delayTime = std::chrono::milliseconds::zero();
}

void AStar::Wait()
{
    do {
        std::this_thread::sleep_for(delayTime);
    } while(paused && !stopRequest);
}

void AStar::RemoveChildrenFromOpen(std::shared_ptr<State>& last)
{
    for(auto it = openSet.begin(); it != openSet.end();)
    {
        std::shared_ptr<State> item = *it;
        if(*item->parent == *last)
        {
            it = openSet.erase(it);
        } else
        {
            it++;
        }
    }
}

void AStar::BacktrackIfNeeded(IProblem& problem, std::shared_ptr<State>& target, std::shared_ptr<State>& last)
{
    //kiedy docelowy stan nie jest nie jest następnikiem ostatniego stanu to idz w gore drzewa stanow i szukaj rozgalezien
    if(target->parent && (*last != *target->parent))
    {
        auto lastDecision = target->parent;
        auto sptr = last;
        auto stateBefore = last;

        while(*sptr != *lastDecision)
        {
            stateBefore = sptr;
            auto realCost = sptr->realCostOfReach + problem.CostOfMove(*sptr, *sptr->parent);

            if(last->hasChildren)
            {
                //jesli stan nie byl ostatni w biezacej galezi, to wracajac otworz galaz do ponownego rozpatrzenia
                RemoveChildrenFromOpen(sptr);
                closedSet.erase(sptr);
            }

            sptr = sptr->parent;
            sptr->realCostOfReach = realCost;
            problem.SelectState(*sptr);

            Wait();
        }

        if(last->hasChildren)
        {
            openSet.erase(last);
            openSet.insert(stateBefore);
        }

        target->realCostOfReach = sptr->realCostOfReach + problem.CostOfMove(*sptr, *target);
    }
}

bool AStar::Resolve(IProblem& problem)
{
    stopRequest = false;
    openSet.clear();
    closedSet.clear();

    auto lastState = problem.GetRootState();
    openSet.insert(lastState);

    while(!openSet.empty() && !stopRequest)
    {
        auto& parent = GetBest(problem, lastState);

        BacktrackIfNeeded(problem, parent, lastState);

        problem.SelectState(*parent);

        if(problem.IsTheGoal(*parent))
        {
            problem.TranslateSolution(ReversePath(parent));
            return true;
        }

        openSet.erase(parent);
        closedSet.insert(parent);
        lastState = parent;

        for (auto& child: problem.GetSuccessors(*parent))
        {
            if(IsInClosed(child))
                continue;

            double cost = parent->realCostOfReach + problem.CostOfMove(*parent, *child);

            if(IsInOpen(child) && (cost < child->realCostOfReach))
            {
                openSet.erase(child);
            }
            else if(IsInClosed(child) && (cost < child->realCostOfReach))
            {
                closedSet.erase(child);
            }
            else if(!IsInClosed(child) && !IsInOpen(child))
            {
                child->realCostOfReach = cost;
                child->costOfReach = parent->costOfReach + problem.CostOfMove(*parent, *child);
                child->estimate = cost + problem.Evaluate(*child);
                openSet.insert(child);
                child->AssignParent(parent);
                parent->hasChildren = true;
            }
        }

        Wait();
    }

    return false;
}

void AStar::Stop()
{
    stopRequest = true;
}

bool AStar::IsPaused() const
{
    return paused;
}

void AStar::SetPaused(bool value)
{
    paused = value;
}

void AStar::SetDelayTime(int miliseconds)
{
    delayTime = std::chrono::milliseconds(miliseconds);
}

int AStar::GetVisitedStatesNumber()
{
    return (int)closedSet.size();
}

std::deque<std::shared_ptr<State>> AStar::ReversePath(std::shared_ptr<State>& node)
{
    std::deque<std::shared_ptr<State>> path;

    auto sptr = node;

    while(sptr)
    {
        path.push_front(sptr);
        sptr = sptr->parent;
    }

    return path;
}

std::shared_ptr<State> AStar::GetBest(IProblem& problem, std::shared_ptr<State> &state)
{
    double minCost = DBL_MAX;
    std::shared_ptr<State> minState;

    for(auto& child:openSet)
    {
        double costOfMove;

        if(child->parent)
            costOfMove = std::abs(state->costOfReach - child->parent->costOfReach);
        else
            costOfMove = problem.CostOfMove(*state, *child);

        auto realCost = state->realCostOfReach + costOfMove;
        child->estimate = realCost + problem.Evaluate(*child);

        double value = child->estimate;
        if(value < minCost)
        {
            minCost = value;
            minState = child;
        }
    }

    return minState;
}

bool AStar::IsInOpen(std::shared_ptr<State>& state)
{
    return std::count(openSet.begin(), openSet.end(), state) > 0;
}

bool AStar::IsInClosed(std::shared_ptr<State>& state)
{
    return closedSet.count(state) > 0;
}
