#include "micromousestate.h"

Point<int> MicromouseState::getPoint() const
{
    return point;
}

void MicromouseState::setPoint(const Point<int> &value)
{
    point = value;
}

int MicromouseState::GetHashCode() const
{
    return point.getX() << 16 | (point.getY() & 0x0000FFFF);
}

bool MicromouseState::operator ==(const State &rhs)
{
    const MicromouseState& ms = static_cast<const MicromouseState&>(rhs);
    return ms.GetHashCode() == this->GetHashCode();
}

bool MicromouseState::operator <(const State &rhs)
{
    const MicromouseState& ms = static_cast<const MicromouseState&>(rhs);
    return ms.GetHashCode() > this->GetHashCode();
}

bool MicromouseState::operator >(const State &rhs)
{
    const MicromouseState& ms = static_cast<const MicromouseState&>(rhs);
    return ms.GetHashCode() < this->GetHashCode();
}

bool MicromouseState::operator !=(const State &rhs)
{
    const MicromouseState& ms = static_cast<const MicromouseState&>(rhs);
    return ms.GetHashCode() != this->GetHashCode();
}

