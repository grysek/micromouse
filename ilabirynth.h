#ifndef ILABIRYNTH_H
#define ILABIRYNTH_H

#include <point.h>
#include <labirynthfield.h>

/*! Interfejs reprezentujacy labirynt */
class ILabirynth
{
public:
    ILabirynth();
    virtual LabirynthField GetField(const Point<int>& point) = 0;
    virtual Point<int> GetBegin() = 0;
    virtual Point<int> GetEnd() = 0;
    virtual int GetSize() = 0;
};

#endif // ILABIRYNTH_H
