#ifndef POINT_H
#define POINT_H

#include <stdlib.h>
#include <string>


template <typename T>
class Point
{
public:

    Point(T x, T y):x(x),y(y) {}

    T getX() const { return x; }
    T getY() const { return y;}
    void setX(T value) { x = value; }
    void setY(T value) { y = value; }

    double Point<T>::Distance(const Point<T> &target)
    {
        return sqrt(pow(this->getX() - target.getX(), 2) + pow(this->getY() - target.getY(), 2));
    }

    bool Point<T>::operator==(const Point<T> &rhs)
    {
        return this->getX() == rhs.getX() && this->getY() == rhs.getY();
    }

    std::string Point<T>::toString()
    {
        std::string str("(" + std::to_string(this->getX()) + "," + std::to_string(this->getY()) + ")");
        return str;
    }

private:
    T x;
    T y;
};

#endif // POINT_H
