#include "hardcodedlabirynth.h"

HardcodedLabirynth::HardcodedLabirynth()
{
    //row 0
    fields[0][0] = true;
    fields[1][0] = true;
    fields[2][0] = true;
    fields[3][0] = true;
    fields[4][0] = true;
    fields[5][0] = false;
    fields[6][0] = true;
    fields[7][0] = true;
    fields[8][0] = true;
    fields[9][0] = true;

    //row 1
        fields[0][1] = true;
        fields[1][1] = false;
        fields[2][1] = false;
        fields[3][1] = false;
        fields[4][1] = true;
        fields[5][1] = false;
        fields[6][1] = false;
        fields[7][1] = false;
        fields[8][1] = false;
        fields[9][1] = true;

        //row 2
        fields[0][2] = true;
        fields[1][2] = false;
        fields[2][2] = true;
        fields[3][2] = false;
        fields[4][2] = true;
        fields[5][2] = true;
        fields[6][2] = true;
        fields[7][2] = true;
        fields[8][2] = false;
        fields[9][2] = true;

        //row 3
        fields[0][3] = true;
        fields[1][3] = true;
        fields[2][3] = true;
        fields[3][3] = false;
        fields[4][3] = false;
        fields[5][3] = false;
        fields[6][3] = false;
        fields[7][3] = true;
        fields[8][3] = false;
        fields[9][3] = true;

        //row 4
        fields[0][4] = true;
        fields[1][4] = false;
        fields[2][4] = true;
        fields[3][4] = false;
        fields[4][4] = true;
        fields[5][4] = true;
        fields[6][4] = false;
        fields[7][4] = false;
        fields[8][4] = false;
        fields[9][4] = true;

        //row 5
        fields[0][5] = true;
        fields[1][5] = false;
        fields[2][5] = false;
        fields[3][5] = false;
        fields[4][5] = false;
        fields[5][5] = true;
        fields[6][5] = false;
        fields[7][5] = true;
        fields[8][5] = true;
        fields[9][5] = true;

        //row 6
        fields[0][6] = true;
        fields[1][6] = false;
        fields[2][6] = true;
        fields[3][6] = true;
        fields[4][6] = true;
        fields[5][6] = true;
        fields[6][6] = false;
        fields[7][6] = false;
        fields[8][6] = false;
        fields[9][6] = true;

        //row 7
        fields[0][7] = true;
        fields[1][7] = false;
        fields[2][7] = true;
        fields[3][7] = false;
        fields[4][7] = false;
        fields[5][7] = false;
        fields[6][7] = true;
        fields[7][7] = true;
        fields[8][7] = false;
        fields[9][7] = true;

        //row 8
        fields[0][8] = true;
        fields[1][8] = false;
        fields[2][8] = false;
        fields[3][8] = false;
        fields[4][8] = true;
        fields[5][8] = false;
        fields[6][8] = true;
        fields[7][8] = false;
        fields[8][8] = false;
        fields[9][8] = true;

        //row 9
        fields[0][9] = true;
        fields[1][9] = true;
        fields[2][9] = true;
        fields[3][9] = true;
        fields[4][9] = true;
        fields[5][9] = false;
        fields[6][9] = true;
        fields[7][9] = true;
        fields[8][9] = true;
        fields[9][9] = true;
}


LabirynthField HardcodedLabirynth::GetField(const Point<int> &point)
{
    if(fields[point.getX()][point.getY()])
        return Wall;
    return Normal;
}

Point<int> HardcodedLabirynth::GetBegin()
{
    Point<int> p(5,0);
    return p;
}

Point<int> HardcodedLabirynth::GetEnd()
{
    Point<int> p(5,9);
    return p;
}

int HardcodedLabirynth::GetSize()
{
    return size;
}
