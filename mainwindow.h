#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <micromouseviewmodel.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void refresh();

    void on_loadButton_clicked();

    void on_startButton_clicked();

    void on_stopButton_clicked();

    void on_horizontalSlider_valueChanged(int value);

    void on_comboBox_currentTextChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    QGraphicsScene* scene;
    QTimer* timer;
    MicromouseViewModel viewModel;

    const int fieldSize = 10;

    void DrawBlock(int x, int y, QColor color);

    void DrawLabirynth();

    void DrawMouse();
};

#endif // MAINWINDOW_H
