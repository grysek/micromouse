#ifndef IMOUSE_H
#define IMOUSE_H

#include <point.h>

/* Interfejs reprezentujacy mysz */
class IMouse
{
public:
    IMouse();
    virtual double Evaluate(Point<int>& destination) = 0;
    virtual bool CanMove(Point<int>& destination) = 0;
    virtual void Move(Point<int>& destination) = 0;
};

#endif // IMOUSE_H
