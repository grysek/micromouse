#ifndef HARDCODEDLABIRYNTH_H
#define HARDCODEDLABIRYNTH_H

#include <ilabirynth.h>

/*! implementacja labiryntu na podstawie dwuwymiarowej tablicy */
class HardcodedLabirynth : public ILabirynth
{
public:
    HardcodedLabirynth();

    // ILabirynth interface
public:
    LabirynthField GetField(const Point<int> &point);
    Point<int> GetBegin();
    Point<int> GetEnd();
    int GetSize();
    static const int size = 10;

private:
    bool fields[size][size] = {{false}};
};

#endif // HARDCODEDLABIRYNTH_H
