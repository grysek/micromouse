#ifndef IPROBLEM_H
#define IPROBLEM_H

#include <stdlib.h>
#include <state.h>
#include <vector>
#include <deque>
#include <memory>

/*! Interfejs definiujacy problem do rozwiazania */
class IProblem
{
public:
    virtual ~IProblem() {}
    virtual std::shared_ptr<State> GetRootState() = 0;
    virtual double Evaluate(State& state) = 0;
    virtual void SelectState(State& state) = 0;
    virtual bool IsTheGoal(State& state) = 0;
    virtual double CostOfMove(State& current, State& target) = 0;
    virtual void TranslateSolution(std::deque<std::shared_ptr<State>> solution) = 0;
    virtual std::vector<std::shared_ptr<State>> GetSuccessors(State& state) = 0;
};

#endif // IPROBLEM_H
