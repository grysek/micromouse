#include "micromouseviewmodel.h"
#include <ilabirynth.h>
#include <hardcodedlabirynth.h>
#include <sstream>
#include <QImage>
#include <bitmaplabirynth.h>
#include <micromouseproblem.h>
#include <virtualmouse.h>

MicromouseViewModel::MicromouseViewModel()
{
    appState = NotStarted;
    l = std::make_unique<HardcodedLabirynth>(HardcodedLabirynth());
    CountWhiteFields();
    SetLabirynthLoadedStatus("Default", l->GetSize());
}

MicromouseViewModel::~MicromouseViewModel()
{
    if(appState != NotStarted)
        worker.detach();
}

void MicromouseViewModel::Run()
{
    if(appState == Started)
    {
        bool paused = !a.IsPaused();
        a.SetPaused(paused);
        if(paused)
        {
            pauseTime = std::chrono::high_resolution_clock::now();
            status = "Paused";
        } else
        {
            auto period = std::chrono::high_resolution_clock::now()-pauseTime;
            startTime += period;
            status = ProcessingStatusStr;
        }
    }
    else
    {
        if(appState == Finished)
            worker.detach();

        prevPoint = nullptr;
        labirynthChanged = true;
        worker = std::thread(&MicromouseViewModel::Resolve, this);
    }
}

void MicromouseViewModel::Resolve()
{
    appState = Started;
    startTime = std::chrono::high_resolution_clock::now();

    mouse = std::make_unique<VirtualMouse>(*l);
    problem = std::make_unique<MicromouseProblem>(*l,*mouse);

    SelectEvalFunc(currentEvalFunc);

    status = ProcessingStatusStr;

    if(a.Resolve(*problem))
    {
        status = "Success!";
        drawSolution = true;
    }
    else
        status = "No solution";

    endTime = std::chrono::high_resolution_clock::now();
    appState = Finished;
}


std::string MicromouseViewModel::GetStatus()
{
    return status;
}

std::string MicromouseViewModel::GetPosition()
{
    if(mouse && mouse->GetCurrentPoint())
        return mouse->GetCurrentPoint()->toString();
    else
        return "-----";
}

std::string MicromouseViewModel::GetTime()
{
    std::ostringstream strs;

    if(appState != NotStarted)
    {
        if(appState == Finished)
        {
            elapsed = endTime-startTime;
        }
        else if(!a.IsPaused())
        {
            elapsed = std::chrono::high_resolution_clock::now()-startTime;
        }

        strs << elapsed.count();
    } else
    {
        strs << "-----";
    }

    return strs.str();
}

std::string MicromouseViewModel::GetPathLength()
{
    if(appState != NotStarted)
    {
        std::ostringstream strs;
        strs << problem->GetCurrentState()->costOfReach;
        return strs.str();
    }

    return "-----";
}

std::string MicromouseViewModel::GetCost()
{
    if(appState != NotStarted)
    {
        std::ostringstream strs;
        strs << problem->GetCurrentState()->realCostOfReach;
        return strs.str();
    }

    return "-----";
}

int MicromouseViewModel::GetVisitedNumber()
{
    if(appState == NotStarted)
        return 0;
    return ((double)a.GetVisitedStatesNumber())/((double)whiteFields)*100;
}

std::string MicromouseViewModel::GetDelayText()
{
    std::ostringstream strs;
    strs << "delay: " << delayTime << "ms";
    return std::string(strs.str());
}

bool MicromouseViewModel::CanStart()
{
    return (appState != Started && l) || a.IsPaused();
}

bool MicromouseViewModel::CanPause()
{
    return appState == Started && !a.IsPaused();
}

bool MicromouseViewModel::CanStop()
{
    return appState == Started;
}

bool MicromouseViewModel::CanLoad()
{
    return appState != Started;
}

void MicromouseViewModel::Stop()
{
    a.Stop();
}

void MicromouseViewModel::SetSpeed(int stepsPerSecond)
{
    delayTime = 1000/stepsPerSecond;
    a.SetDelayTime(delayTime);
}

bool MicromouseViewModel::LabirynthChanged()
{
    //draw only one time
    if(labirynthChanged)
    {
        labirynthChanged = false;
        return true;
    }
    return false;
}

bool MicromouseViewModel::PositionChanged()
{
    if(!(mouse && mouse->GetCurrentPoint()))
        return false;

    if(!currPoint || !(*mouse->GetCurrentPoint() == *currPoint))
    {
        prevPoint = currPoint;
        currPoint = mouse->GetCurrentPoint();
        return true;
    }

    return false;
}

Point<int> &MicromouseViewModel::GetCurrentPoint()
{
    return *currPoint;
}

Point<int> &MicromouseViewModel::GetPreviousPoint()
{
    return *prevPoint;
}

bool MicromouseViewModel::HasPreviousPoint()
{
    return appState != NotStarted && prevPoint;
}

bool MicromouseViewModel::ShouldDrawSolution()
{
    if(appState == Finished && drawSolution)
    {
        drawSolution = false;
        return true;
    }

    return false;
}

std::deque<std::shared_ptr<Point<int>>> MicromouseViewModel::GetSolution()
{
    return problem->GetResult();
}

bool MicromouseViewModel::ShouldReloadEvalFuncOptions()
{
    if(reloadHeurOptions)
    {
        reloadHeurOptions = false;
        return true;
    }
    return false;
}

std::vector<std::string> MicromouseViewModel::GetEvalFuncOptions()
{
    std::vector<std::string> options;
    options.push_back(HeurOptDistStr);
    options.push_back(HeurOptRandStr);
    return options;
}

void MicromouseViewModel::SelectEvalFunc(std::string name)
{
    currentEvalFunc = name;

    if(!mouse)
        return;

    if(name == HeurOptRandStr)
        mouse->UseRandomEvalFunc();
    else
        mouse->UseDistanceEvalFunc();
}

ILabirynth &MicromouseViewModel::GetLabirynth()
{
    return *l;
}

bool MicromouseViewModel::LoadLabirynthFromFile(QString path)
{
    QImage bitmap(path);
    try
    {
        l = std::make_unique<BitmapLabirynth>(bitmap);
        CountWhiteFields();
        SetLabirynthLoadedStatus(path.toStdString(), l->GetSize());
        labirynthChanged = true;
    }
    catch (const std::invalid_argument& e)
    {
        std::ostringstream strs;
        strs << "Could not load '" << path.toStdString() << "'. " << e.what();
        status = strs.str();
        return false;
    }
    return true;
}

void MicromouseViewModel::SetLabirynthLoadedStatus(std::string name, int size)
{
    std::ostringstream strs;
    strs << name << " labirynth loaded. Size: " << size << "x" << size;
    status = strs.str();
}

void MicromouseViewModel::CountWhiteFields()
{
    whiteFields = 0;
    for(int x = 0; x < l->GetSize(); x++)
    {
        for(int y = 0; y < l->GetSize(); y++)
        {
            if(l->GetField(Point<int>(x,y)) == Normal)
                whiteFields++;
        }
    }
}
