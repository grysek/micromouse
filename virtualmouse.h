#ifndef VIRTUALMOUSE_H
#define VIRTUALMOUSE_H

#include <imouse.h>
#include <ilabirynth.h>
#include <point.h>
#include <memory>

/*! implementacja wirtualnej myszy */
class VirtualMouse : public IMouse
{
public:
    enum EvalFunc {
        Distance,Random
    };

private:
    ILabirynth& labirynth;
    std::shared_ptr<Point<int>> currentPoint;
    EvalFunc evalFunc;

public:
    VirtualMouse(ILabirynth& labirynth):labirynth(labirynth) { currentPoint = std::make_shared<Point<int>>(labirynth.GetBegin()); }
    std::shared_ptr<Point<int>> GetCurrentPoint();
    void UseDistanceEvalFunc();
    void UseRandomEvalFunc();

    // IMouse interface
public:
    double Evaluate(Point<int> &destination);
    bool CanMove(Point<int> &destination);
    void Move(Point<int> &destination);
};

#endif // VIRTUALMOUSE_H
