#ifndef BITMAPLABIRYNTH_H
#define BITMAPLABIRYNTH_H

#include <ilabirynth.h>
#include <memory>
#include <QImage>

/*! implementacja labiryntu ktorego zrodlem jest bitmapa */
class BitmapLabirynth : public ILabirynth
{
public:
    BitmapLabirynth(QImage source);

    // ILabirynth interface
public:
    LabirynthField GetField(const Point<int> &point);
    Point<int> GetBegin();
    Point<int> GetEnd();
    int GetSize();

private:
    std::unique_ptr<QImage> source;
    bool ValidateSize(int size);
    std::unique_ptr<Point<int>> begin, end;
    Point<int> BitmapLabirynth::GetRandomPoint();
    void GetRandomBegin();
    void GetRandomEnd();
};

#endif // BITMAPLABIRYNTH_H
