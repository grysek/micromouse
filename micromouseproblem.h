#ifndef MICROMOUSEPROBLEM_H
#define MICROMOUSEPROBLEM_H

#include <iproblem.h>
#include <ilabirynth.h>
#include <imouse.h>
#include <state.h>
#include <micromousestate.h>

/* Implementacja problemu micromouse */
class MicromouseProblem : public IProblem
{
public:
    MicromouseProblem(ILabirynth& labirynth, IMouse& mouse):labirynth(labirynth),mouse(mouse) {}
    ~MicromouseProblem() {}
    std::deque<std::shared_ptr<Point<int>>> GetResult();
    std::shared_ptr<MicromouseState> GetCurrentState();
    // IProblem interface
public:
    std::shared_ptr<State> GetRootState();
    double Evaluate(State &state);
    bool IsTheGoal(State &state);
    double CostOfMove(State &current, State &target);
    void TranslateSolution(std::deque<std::shared_ptr<State>> solution);
    std::vector<std::shared_ptr<State>> GetSuccessors(State &state);
    void SelectState(State &state);

private:
    ILabirynth& labirynth;
    IMouse& mouse;
    std::deque<std::shared_ptr<Point<int>>> result;
    std::shared_ptr<MicromouseState> current;
    void GoIfPossible(Point<int>& point, std::vector<std::shared_ptr<State>>& options);
    // IProblem interface
public:

};

#endif // MICROMOUSEPROBLEM_H
