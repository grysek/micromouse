#ifndef STATE_H
#define STATE_H

#include <stdlib.h>
#include <memory>

/*! interfejs stanu problemu */
class State
{
public:
    double costOfReach = 0;
    double realCostOfReach = 0;
    double estimate = 0;
    double backtrackingCost = 0;
    bool hasChildren = false;
    std::shared_ptr<State> parent;

    State() {}
    virtual ~State() {}
    void AssignParent(std::shared_ptr<State>& node);

    virtual bool operator ==(const State& rhs) = 0;
    virtual bool operator !=(const State& rhs) = 0;
    virtual bool operator <(const State& rhs) = 0;
    virtual bool operator >(const State& rhs) = 0;
};

#endif // STATE_H
