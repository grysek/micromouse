#ifndef MICROMOUSESTATE_H
#define MICROMOUSESTATE_H

#include <state.h>
#include <point.h>

/*! stan problemu micromouse */
class MicromouseState : public State
{
public:
    MicromouseState(Point<int> point):point(point) {}

    Point<int> getPoint() const;
    void setPoint(const Point<int> &value);

private:
    Point<int> point;
    int GetHashCode() const;

    // State interface

public:
    bool operator ==(const State &rhs);
    bool operator !=(const State &rhs);
    bool operator <(const State &rhs);
    bool operator >(const State &rhs);
};

#endif // MICROMOUSESTATE_H
