#ifndef MICROMOUSEVIEWMODEL_H
#define MICROMOUSEVIEWMODEL_H

#include <astar.h>
#include <string>
#include <micromouseproblem.h>
#include <hardcodedlabirynth.h>
#include <virtualmouse.h>
#include <memory>
#include <thread>
#include <chrono>
#include <deque>
#include <vector>
#include <QString>

/*! Klasa koorydnujaca logike programu z jego widokiem */
class MicromouseViewModel
{
public:
    MicromouseViewModel();
    ~MicromouseViewModel();

    void Run();
    std::string GetStatus();
    std::string GetPosition();
    std::string GetTime();
    std::string GetPathLength();
    std::string GetCost();
    int GetVisitedNumber();
    std::string GetDelayText();

    bool CanStart();
    bool CanPause();
    bool CanStop();
    bool CanLoad();
    void Stop();
    void SetSpeed(int stepsPerSecond);

    bool LabirynthChanged();
    bool PositionChanged();
    Point<int>& GetCurrentPoint();
    Point<int>& GetPreviousPoint();
    bool HasPreviousPoint();
    bool ShouldDrawSolution();
    std::deque<std::shared_ptr<Point<int>>> GetSolution();

    bool ShouldReloadEvalFuncOptions();
    std::vector<std::string> GetEvalFuncOptions();
    void SelectEvalFunc(std::string name);

    ILabirynth& GetLabirynth();
    bool LoadLabirynthFromFile(QString path);

    void Resolve();

    void LoadLabirynth();

private:
    const std::string ProcessingStatusStr = "Trying to find a solution...";
    const std::string HeurOptDistStr = "distance to end (optimal)";
    const std::string HeurOptRandStr = "random";

    enum AppState {
        NotStarted,Started,Finished
    };

    AStar a;

    std::unique_ptr<ILabirynth> l;
    std::unique_ptr<MicromouseProblem> problem;
    std::unique_ptr<VirtualMouse> mouse;

    std::string status;
    std::string time;
    std::thread worker;

    AppState appState;
    std::chrono::time_point<std::chrono::high_resolution_clock> startTime;
    std::chrono::time_point<std::chrono::high_resolution_clock> endTime;
    std::chrono::time_point<std::chrono::high_resolution_clock> pauseTime;
    std::chrono::duration<double> elapsed;

    bool labirynthChanged = true;
    bool drawSolution = false;
    bool reloadHeurOptions = true;
    std::string currentEvalFunc;
    int delayTime = 0;
    int whiteFields = 0;

    std::shared_ptr<Point<int>> currPoint;
    std::shared_ptr<Point<int>> prevPoint;
    void SetLabirynthLoadedStatus(std::string name, int size);
    void CountWhiteFields();
};

#endif // MICROMOUSEVIEWMODEL_H
